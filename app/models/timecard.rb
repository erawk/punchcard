# frozen_string_literal: true

class Timecard < ApplicationRecord
  belongs_to :user

  validates :clocked_in_at, presence: true
  validate :validate_clocked_out_at

  scope :active, -> { where(clocked_out_at: nil) }
  scope :complete, -> { where.not(clocked_out_at: nil) }
  scope :recent, -> { order(updated_at: :desc) }

  def clock_out!(time: Time.zone.now)
    raise ArgumentError, 'Already has a clocked_out_at' if clocked_out_at.present?

    update!(clocked_out_at: time)
  end

  private

  def validate_clocked_out_at
    return true if clocked_out_at.blank?
    return true if clocked_out_at > clocked_in_at

    errors.add(:clocked_out_at, 'must be later than clock in')
  end
end

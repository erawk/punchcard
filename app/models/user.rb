# frozen_string_literal: true

class User < ApplicationRecord
  attr_accessor :password

  has_many :timecards

  validates :name, presence: true
  validates :email, presence: true, uniqueness: true, format: /.+@.+\..+/i
  validates :password, length: { minimum: 6 }, on: :create

  before_save :encrypt_password

  def self.sign_in(email:, password:)
    user = find_by(email: email)
    return unless user
    return if user.password_hash != BCrypt::Engine.hash_secret(password, user.password_salt)

    user
  end

  def active_timecard
    timecards.active.first
  end

  def punch_clock!
    if active_timecard
      active_timecard.clock_out!
    else
      timecards.create(clocked_in_at: Time.zone.now)
    end
  end

  private

  def encrypt_password
    return if password.blank?

    self.password_salt = BCrypt::Engine.generate_salt
    self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
  end
end

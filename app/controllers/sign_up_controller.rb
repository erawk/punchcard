# frozen_string_literal: true

class SignUpController < ApplicationController
  before_action :must_be_signed_out

  def index
    @user = User.new
  end

  def create
    @user = User.new(user_params)

    if @user.save
      sign_in(user: @user)
      redirect_to '/'
    else
      render :index
    end
  end

  def user_params
    params.require(:user).permit(:name, :email, :password)
  end
end

# frozen_string_literal: true

class LoginController < ApplicationController
  before_action :must_be_signed_out, except: :delete

  def index
    @user = User.new
  end

  def create
    @user = User.sign_in(email: user_params[:email], password: user_params[:password])

    if @user
      sign_in(user: @user)
      redirect_to '/'
    else
      @user = User.new(email: user_params[:email], password: user_params[:password])
      @user.errors.add(:email, 'Email or password not found.')
      render :index
    end
  end

  def delete
    session[:user_id] = nil
    redirect_to login_path
  end

  def user_params
    params.require(:user).permit(:email, :password).to_h.symbolize_keys
  end
end

# frozen_string_literal: true

class AdminController < ApplicationController
  before_action :must_be_admin

  def index
    @timecards = Timecard.eager_load(:user).all # TODO: Paginate me
  end
end

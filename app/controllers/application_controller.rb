# frozen_string_literal: true

class ApplicationController < ActionController::Base
  def current_user
    @current_user ||= User.find_by(id: session[:user_id])
  end
  helper_method :current_user

  def must_be_admin
    return if signed_in? && current_user.admin?

    redirect_to '/'
  end

  def must_be_signed_in
    return if signed_in?

    redirect_to login_path
  end

  def must_be_signed_out
    return if signed_out?

    redirect_to '/'
  end

  def signed_in?
    current_user.present?
  end

  def signed_out?
    !signed_in?
  end

  def sign_in(user:)
    session[:user_id] = user.id
  end
end

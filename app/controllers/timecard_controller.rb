# frozen_string_literal: true

class TimecardController < ApplicationController
  before_action :must_be_signed_in

  include ActionView::Helpers::DateHelper

  def index
    if active_timecard = current_user.active_timecard
      @timecard = active_timecard
      @msg = <<~MSG
        Hope you're having a good day.
        You started at #{active_timecard.clocked_in_at.to_formatted_s(:long_tz)},
        which was #{distance_of_time_in_words(active_timecard.clocked_in_at, Time.zone.now)} ago.
      MSG
      @submit_text = 'Clock out!'
    else
      @timecard = current_user.timecards.new
      @msg = 'Ready to start your day? ⏲️'
      @submit_text = 'Clock in!'
    end

    @timecards = current_user.timecards.complete.recent.first(10)
  end

  def create
    current_user.punch_clock!

    redirect_to '/'
  end

  def timecard_params
    params.require(:timecard).permit(:clock_in_at, :clock_out_at).to_h.symbolize_keys
  end
end

# PUNCHCARD APP

A simple application to keep track of people clocking in and out of a job for timesheet purposes.

## Infrastructure

- Ruby 2.4.3
- Rails 5.2.2
- MySQL
- Puma
- Rspec, FactoryBot
- Bootstrap 4
- Haml

## Application layout

- User (for authentication, has_many timecards)
- Timecard (belongs_to user, clock_in, clock_out)

## Flows

- User should be able to sign up
- User should be able to sign in
- User should be able to clock in for only their timecards
- User should be able to clock out for only their timecards
- User shold be able to see only their timecards
- Administrators should be able to view all timecards
- TODO: Administrators should be able to edit all timecards

## Testing

- User should have many timecards
- Timecards should only have a single owner
- No more than one open timecard per user
- No overlapping timecards per user

## Revisit

- Account for user's timezone
- Use devise next time
- Consider a role-based authorization framework for hierarchy of timesheet views
- Use active admin for admin views
- Paginate admin view
- Paginate user timecards

## Seeded users

- Alice (alice@example.com, alice1010)
- Bob (bob@example.com, bob1010)
- Cassie (cassie@example.com, cassie1010, admin rights)

## Install instructions

- `$ git clone https://github.com/erawk/punchcard.git`
- `$ bundle install`
- ensure mysql user `punchcard` exists with password `punchcard`
- `$ bundle exec spring rails db:create db:migrate db:seed`
- `$ bundle exec spring rails server`
- should be able to naviate to `http://127.0.0.1:3000` and login with any of the credentials above or sign up

## Deployment

- Currently deployed to `https://mypunchcardapp.heroku.com`
- Uses Hobby plan on Heroku with JawsDB MySQL add-on

# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'validations' do
    subject { FactoryBot.create(:user) }

    it { should validate_presence_of(:email) }
    it { should validate_uniqueness_of(:email) }
    it { should allow_values('foo@bar.com', 'baz+you@lux.ca').for(:email) }
    it { should_not allow_values('foobar.com', 'bazyouca').for(:email) }
    # TODO: Add more email permutations to catch commonly misspelt domains (gmail.con)

    it { should validate_length_of(:password).is_at_least(6).on(:create) }
  end

  describe 'associations' do
    it { should have_many(:timecards) }
  end

  describe '#encrypt_password' do
    context 'with new user' do
      it 'creates password hash' do
        expect_any_instance_of(User).to receive(:encrypt_password).and_call_original
        user = User.new(
          name: Faker::Name.name,
          email: Faker::Internet.email,
          password: 'foobar',
        )
        user.save
        expect(user.password_hash.present?).to eq true
      end
    end

    context 'with old user' do
      let(:user) { FactoryBot.create(:user) }

      it 'does nothing' do
        expect_any_instance_of(User).to receive(:encrypt_password).twice.and_call_original
        old_password_hash = user.password_hash
        user.password = nil
        user.save
        expect(old_password_hash).to eq user.password_hash
      end
    end
  end

  describe '#punch_clock!' do
    let(:user) { FactoryBot.create(:user) }

    context 'with an active timecard' do
      let(:timecard) { FactoryBot.create(:timecard, user: user, clocked_in_at: 1.hour.ago) }

      it 'sets a clock out time' do
        expect do
          user.punch_clock!
        end.to(change { timecard.reload.clocked_out_at })
      end
    end

    context 'without an active timecard' do
      it 'creates a new timecard' do
        expect do
          user.punch_clock!
        end.to change { user.timecards.count }.by 1
      end
    end
  end

  describe '.sign_in' do
    let(:user) { FactoryBot.create(:user, password: 'password') }

    context 'with valid credentials' do
      it 'authenticates a user' do
        expect(User.sign_in(email: user.email, password: 'password')).to eq user
      end
    end

    context 'with invalid email' do
      it 'returns nil' do
        expect(User.sign_in(email: 'nope', password: 'password')).to eq nil
      end
    end

    context 'with invalid password' do
      it 'returns nil' do
        expect(User.sign_in(email: user.email, password: 'nope')).to eq nil
      end
    end
  end
end

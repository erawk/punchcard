# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Timecard, type: :model do
  it { should validate_presence_of(:clocked_in_at) }

  it { should belong_to(:user) }

  describe '#validate_clocked_out_at' do
    let(:user) { FactoryBot.create(:user) }

    context 'without a clocked_out_at' do
      it 'should be valid' do
        timecard = user.timecards.new(clocked_in_at: Time.zone.now)
        expect(timecard.valid?).to eq true
      end
    end

    context 'with a clocked_out_at after clocked_in_at' do
      it 'should be valid' do
        timecard = user.timecards.new(
          clocked_in_at: Time.zone.now,
          clocked_out_at: 1.hour.from_now,
        )
        expect(timecard.valid?).to eq true
      end
    end

    context 'with a clocked_out_at before clocked_in_at' do
      it 'should be invalid' do
        timecard = user.timecards.new(
          clocked_in_at: Time.zone.now,
          clocked_out_at: 1.hour.ago,
        )
        expect(timecard.valid?).to eq false
      end
    end

    context 'with a clocked_out_at exactly at clocked_in_at' do
      it 'should be invalid' do
        now = Time.zone.now
        timecard = user.timecards.new(
          clocked_in_at: now,
          clocked_out_at: now,
        )
        expect(timecard.valid?).to eq false
      end
    end
  end

  describe '#clock_out!' do
    let(:user) { FactoryBot.create(:user) }

    context 'with a timecard' do
      it 'sets a clocked_out_at' do
        timecard = user.timecards.create(clocked_in_at: Time.zone.now)
        later = Time.zone.parse(1.hour.from_now.to_s).change(usec: 0)
        expect { timecard.clock_out!(time: later) }.to_not raise_exception
        expect(Time.zone.parse(timecard.clocked_out_at.to_s).change(usec: 0)).to eq later
      end
    end
  end
end

# frozen_string_literal: true

FactoryBot.define do
  factory :timecard do
    user
    clocked_in_at { Time.zone.now }
  end

  factory :user do
    name { Faker::Name.name }
    email { Faker::Internet.email }
    password { Faker::Internet.password }
  end
end

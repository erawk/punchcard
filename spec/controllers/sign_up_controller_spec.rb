# frozen_string_literal: true

require 'rails_helper'

RSpec.describe SignUpController, type: :controller do
  describe 'GET #index' do
    it 'returns http success' do
      get :index
      expect(response).to have_http_status(:success)
    end
  end

  describe 'POST #create' do
    context 'with valid user input' do
      it 'creates user and redirects home' do
        expect do
          post :create, params: {
            user: {
              name: Faker::Name.name,
              email: Faker::Internet.email,
              password: Faker::Internet.password,
            },
          }
        end.to change { User.count }.by 1
        expect(response).to redirect_to '/'
      end
    end

    context 'with invalid user input' do
      it 'renders errors' do
        expect do
          post :create, params: {
            user: {
              name: Faker::Name.name,
              email: 'not email',
              password: Faker::Internet.password,
            },
          }
        end.to change { User.count }.by 0
        expect(response).to render_template(:index)
      end
    end
  end
end

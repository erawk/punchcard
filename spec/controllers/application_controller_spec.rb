# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ApplicationController, type: :controller do
  describe '#must_be_admin' do
    context 'with signed in admin user' do
      let(:admin_user) { FactoryBot.create(:user, admin: true) }

      it 'does not redirect' do
        expect(controller).to receive(:signed_in?).and_return(true)
        expect(controller).to receive(:current_user).and_return(admin_user)
        expect(controller.send(:must_be_admin)).to eq nil
      end
    end
  end

  describe '#must_be_signed_in' do
    context 'with signed in user' do
      it 'does not redirect' do
        expect(controller).to receive(:signed_in?).and_return(true)
        expect(controller.send(:must_be_signed_in)).to eq nil
      end
    end
  end

  describe '#must_be_signed_out' do
    context 'with signed out user' do
      it 'does not redirect' do
        expect(controller).to receive(:signed_out?).and_return(true)
        expect(controller.send(:must_be_signed_out)).to eq nil
      end
    end
  end
end

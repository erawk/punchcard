# frozen_string_literal: true

require 'rails_helper'

RSpec.describe LoginController, type: :controller do
  describe 'GET #index' do
    it 'returns http success' do
      get :index
      expect(response).to have_http_status(:success)
    end
  end

  describe 'POST #create' do
    context 'with valid user input' do
      let(:user) { FactoryBot.create(:user, password: 'password') }

      it 'finds user and redirects home' do
        post :create, params: {
          user: {
            email: user.email,
            password: 'password',
          },
        }
        expect(response).to redirect_to('/')
        expect(assigns(:user)).to eq user
      end
    end

    context 'with invalid user creds' do
      it 'renders errors' do
        email = Faker::Internet.email
        post :create, params: {
          user: {
            email: email,
            password: Faker::Internet.password,
          },
        }
        expect(response).to render_template(:index)
        expect(assigns(:user).email).to eq email
      end
    end
  end

  describe 'GET #delete' do
    context 'with a session' do
      it 'should clear the associated user' do
        @request.session[:user_id] = 1
        get :delete
        expect(@request.session[:user_id]).to eq nil
      end
    end
  end
end

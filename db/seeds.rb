# frozen_string_literal: true

# Create simple users
alice = User.create!(name: 'Alice', email: 'alice@example.com', password: 'alice1010')
bob = User.create!(name: 'Bob', email: 'bob@example.com', password: 'bob1010')

# Create some sample timecards
[alice, bob].each do |user|
  rand(1..2).downto(0) do |n|
    clock_in_at = n.days.ago
    clock_out_at = clock_in_at + rand(1..5).minutes
    user.timecards.create!(
      clocked_in_at: clock_in_at,
      clocked_out_at: clock_out_at,
    )
  end
end

# Create an admin user
User.create!(name: 'Cassie', email: 'cassie@example.com', password: 'cassie1010', admin: true)

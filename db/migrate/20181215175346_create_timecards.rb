# frozen_string_literal: true

class CreateTimecards < ActiveRecord::Migration[5.2]
  def change
    create_table :timecards do |t|
      t.references :user, foreign_key: true
      t.datetime :clocked_in_at, null: false
      t.datetime :clocked_out_at

      t.timestamps
    end
  end
end

# frozen_string_literal: true

Time::DATE_FORMATS[:long_tz] = '%B %d, %Y %H:%M %Z'

# frozen_string_literal: true

Rails.application.routes.draw do
  get '/', to: 'timecard#index', as: :timecard
  post '/timecard', to: 'timecard#create', as: :timecard_punch

  get '/sign_up', to: 'sign_up#index', as: :sign_up
  post '/sign_up', to: 'sign_up#create'

  get '/login', to: 'login#index', as: :login
  post '/login', to: 'login#create'
  get '/logout', to: 'login#delete', as: :logout

  get '/admin', to: 'admin#index', as: :admin
end
